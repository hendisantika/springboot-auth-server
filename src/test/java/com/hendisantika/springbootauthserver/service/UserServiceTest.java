package com.hendisantika.springbootauthserver.service;

import com.hendisantika.springbootauthserver.model.User;
import com.hendisantika.springbootauthserver.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-15
 * Time: 07:20
 */
public class UserServiceTest {
    @Mock
    private UserRepository userRepo;

    private UserService userService;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        userService = new UserService(userRepo);
    }

    @Test
    public void testGivenUserServiceWhenLoadingUserByUsernameThenReturnCorrectUser() {
        User user = new User();
        user.setId(1L);
        user.setEmail("john@example.com");

        given(userRepo.findOneByEmail("john@example.com")).willReturn(Optional.of(user));

        assertThat(userService.loadUserByUsername("john@example.com"), is(user));
    }
}