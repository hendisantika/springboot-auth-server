package com.hendisantika.springbootauthserver.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-14
 * Time: 07:17
 */
@RequestMapping("/")
public class IndexController {
    @GetMapping
    public String index() {
        return "This is Spring Boot Auth Server! " + new Date();
    }
}
