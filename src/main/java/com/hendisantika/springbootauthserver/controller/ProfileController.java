package com.hendisantika.springbootauthserver.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 08:22
 */
@Controller
public class ProfileController {

    /**
     * <p>
     * Return profile page.
     * </p>
     */
    @GetMapping("/profile")
    public String profile() {
        return "profile";
    }

}