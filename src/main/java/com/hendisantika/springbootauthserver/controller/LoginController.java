package com.hendisantika.springbootauthserver.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 08:20
 */
@Controller
public class LoginController {

    /**
     * <p>
     * Return home page.
     * </p>
     */
    @GetMapping("/")
    public String home() {
        return "index";
    }

    /**
     * <p>
     * Return login page or redirect user to profile if already logged in.
     * </p>
     */
    @GetMapping("/login")
    public String login() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken)) {

            // The user is logged in
            return "redirect:/profile";
        }

        return "login";
    }

}