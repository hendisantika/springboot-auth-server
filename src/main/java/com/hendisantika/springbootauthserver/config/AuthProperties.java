package com.hendisantika.springbootauthserver.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 08:11
 */
@Data
@Configuration
//@EnableConfigurationProperties("auth")
@ConfigurationProperties("auth")
public class AuthProperties {

    private String redirectionUrl;
    private String emailFrom;
    private String corsAllowedOrigins;

}