package com.hendisantika.springbootauthserver.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.stereotype.Service;

import java.util.Collection;


/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 08:02
 */
@Slf4j
@Service
public class TokenServiceImpl implements TokenService {

    private final TokenStore tokenStore;

    /**
     * <p>
     * TokenService constructor.
     * </p>
     */
    public TokenServiceImpl(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    @Override
    public void revokeTokens(String username) {
        log.debug("Revoking tokens for {}", username);

        if (!(tokenStore instanceof JdbcTokenStore)) {
            log.debug("Token store is not instance of JdbcTokenStore. Cannot revoke tokens!");

            return;
        }

        Collection<OAuth2AccessToken> tokens = ((JdbcTokenStore) tokenStore).findTokensByUserName(username);

        for (OAuth2AccessToken token : tokens) {
            log.debug("Revoking access token {}", token);
            tokenStore.removeAccessToken(token);

            log.debug("Revoking refresh token {}", token.getRefreshToken());
            tokenStore.removeRefreshToken(token.getRefreshToken());
        }

    }

}

