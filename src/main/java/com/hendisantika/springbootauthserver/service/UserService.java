package com.hendisantika.springbootauthserver.service;

import com.hendisantika.springbootauthserver.model.User;
import com.hendisantika.springbootauthserver.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 08:01
 */
@Slf4j
@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        log.debug("Loading user details for username: {}", username);

        Optional<User> optionalUser = userRepository.findOneByEmail(username);

        if (!optionalUser.isPresent()) {
            log.error("User not found!");

            throw new UsernameNotFoundException("E-mail address not found.");
        }

        return optionalUser.get();
    }
}
