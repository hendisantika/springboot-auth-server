package com.hendisantika.springbootauthserver.service;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 08:02
 */
public interface TokenService {
    /**
     * <p>
     * Revoke access and refresh tokens for user with given username.
     * </p>
     *
     * @param username String
     */
    void revokeTokens(String username);
}
