package com.hendisantika.springbootauthserver.service;

import com.hendisantika.springbootauthserver.model.User;

import java.util.Locale;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 07:59
 */
public interface AccountService {
    /**
     * <p>
     * Register given user.
     * </p>
     */
    void registerUser(User user, Locale locale);

    /**
     * <p>
     * Confirm given user based on token and set his password.
     * </p>
     */
    void confirmUser(String token, String password);

    /**
     * <p>
     * Return true if this user is already registered in the system.
     * </p>
     */
    boolean isUserRegistered(User user);

    /**
     * <p>
     * Get user for given registration token.
     * </p>
     */
    Optional<User> getUserForToken(String token);

    /**
     * <p>
     * Reset password for given user.
     * </p>
     */
    void resetPassword(User user, Locale locale);

    /**
     * <p>
     * Change password for given user.
     * </p>
     */
    boolean changePassword(String username, String oldPassword, String newPassword);

    /**
     * <p>
     * Change email for given user.
     * </p>
     */
    boolean changeEmail(String username, String password, String newEmail, Locale locale);

    /**
     * <p>
     * Verify email for given user.
     * </p>
     */
    void verifyEmail(User user);
}
