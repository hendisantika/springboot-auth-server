package com.hendisantika.springbootauthserver.model;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 07:57
 */
public enum Role {
    USER, ADMIN
}
