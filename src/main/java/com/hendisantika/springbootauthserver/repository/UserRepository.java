package com.hendisantika.springbootauthserver.repository;

import com.hendisantika.springbootauthserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-auth-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-14
 * Time: 07:58
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Find a user by email.
     *
     * @param email the user's email
     * @return user which contains the user with the given email or null.
     */
    Optional<User> findOneByEmail(String email);

    /**
     * Find a user by confirmation token.
     *
     * @param confirmationToken generated for registration confirmation
     * @return user associated with this confirmation token
     */
    Optional<User> findByConfirmationToken(String confirmationToken);

    /**
     * Find a user by ID.
     *
     * @param id the user's ID
     * @return User returns an Optional User object which contains the user or null.
     */
    Optional<User> findById(Long id);
}